import './button.scss';

export default function Button(props) {
    const { text, onClick, className } = props;
    return(
        <>
            <button className={className} onClick={onClick}>{text}</button>
        </>
    )
}