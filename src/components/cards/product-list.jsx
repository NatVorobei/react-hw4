import './product-card';
import ProductCard from "./product-card";
import Modal from "../modals";
import { useDispatch, useSelector } from 'react-redux';
import { closeModal, openModal } from '../../redux/action/modals';

export default function ProductList(props) {
    const dispatch = useDispatch();
    const isModalOpened = useSelector(state => state.modalsReducer.isModalOpened);
    const modalData = useSelector(state => state.modalsReducer.modalData);

    function openAddToCartModal(productID) {
        dispatch(openModal({
            action: 'add',
            productID,
            text: 'This product will be added to your cart'
        }));
    }    
    
    function openRemoveFromCartModal(productID) {
        dispatch(openModal({
            action: 'remove',
            productID,
            text: 'This product will be removed from your cart'
        }));
    }

    function addToFavs(productID){
        props.onAddProductToFavs(productID);
    }

    function removeFromFavs(productID){
        props.onRemoveProductFromFavs(productID);
    }

    function onCloseModal() {
        dispatch(closeModal());
    }

    function submitModal() {
        if (modalData.action === 'add') {
            props.onAddProductToCart(modalData.productID)
        } else if (modalData.action === 'remove') {
            props.onRemoveProductFromCart(modalData.productID)
        }
        onCloseModal(); 
    }
    
    return (
        <>
            <div className="products">
                {props.products.map(product => (
                    <ProductCard
                        key={product.id}
                        name={product.name}
                        price={product.price}
                        image={product.image}
                        sku={product.sku}
                        color={product.color}
                        isAddedToCart={props.cartItems.indexOf(product.sku) >= 0}
                        isAddedToFavourites={props.favourites.indexOf(product.sku) >= 0}
                        onAddToCart={() => openAddToCartModal(product.sku)}
                        onRemoveFromToCart={() => openRemoveFromCartModal(product.sku)}
                        onAddToFavourite={() => addToFavs(product.sku)}
                        onRemoveFromFavourite={() => removeFromFavs(product.sku)}
                    />
                ))}
            </div>
            <Modal
                show={isModalOpened}
                text={modalData.text} 
                onClose={() => onCloseModal()} 
                onSubmit={() => {submitModal()}} 
                />
        </>
    );
}

