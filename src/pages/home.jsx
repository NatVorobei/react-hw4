import { useSelector } from "react-redux";
import ProductList from "../components/cards/product-list";

export function Home(){
    // const products = useSelector(state => state.productsReducer.products);
    return (
        <>
            <h1>Home page</h1>
            <ProductList
                // products={products}
                onAddProductToCart={(productID) => this.addProductToCart(productID)}
                onRemoveProductFromCart={(productID) => this.removeProductFromCart(productID)}
                onAddProductToFavs={(productID) => this.addProductToFavourites(productID)}
                onRemoveProductFromFavs={(productID) => this.removeProductFromFavourites(productID)}  
                />
        </>
    )
}