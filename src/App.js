import './App.css';
import { Routes, Route } from 'react-router-dom';
import { useEffect } from 'react';
import ProductList from './components/cards/product-list';
import Header from './components/header/header';
import { Favourites } from './pages/favourites';
import { Cart } from './pages/cart';
import { useDispatch, useSelector } from 'react-redux';
import { addToCart, addToFavourites, getProductsAsync, removeFromCart, removeFromFavourites } from './redux/action/products';


export default function App() {
  const dispatch = useDispatch();
  const products = useSelector(state => state.productsReducer.products);
  const cartItems = useSelector(state => state.productsReducer.cartItems);
  const favourites = useSelector(state => state.productsReducer.favourites);

  useEffect(() => {
    dispatch(getProductsAsync());
  }, [dispatch]);

  function addProductToCart(productID) {
    dispatch(addToCart(productID));
  }

  function addProductToFavourites(productID) {
    dispatch(addToFavourites(productID));
  }

  function removeProductFromCart(productID) {
    dispatch(removeFromCart(productID));
  }

  function removeProductFromFavourites(productID) {
    dispatch(removeFromFavourites(productID));
  }

  return (
    <>
      <Header cartCount={cartItems ? cartItems.length : 0} favouriteCount={favourites ? favourites.length : 0}/>
      <Routes>
        <Route 
          path="/" 
          element={<ProductList 
            products={products}
            cartItems={cartItems}
            favourites={favourites}
            onAddProductToCart={(productID) => addProductToCart(productID)}
            onRemoveProductFromCart={(productID) => removeProductFromCart(productID)}
            onAddProductToFavs={(productID) => addProductToFavourites(productID)}
            onRemoveProductFromFavs={(productID) => removeProductFromFavourites(productID)}  
            />}
          />
        <Route
          path="/cart" 
          element={<Cart 
            products={products.filter((product) => cartItems.indexOf(product.sku) >= 0)}
            cartItems={cartItems}
            favourites={favourites}
            onAddProductToCart={(productID) => addProductToCart(productID)}
            onRemoveProductFromCart={(productID) => removeProductFromCart(productID)}
            onAddProductToFavs={(productID) => addProductToFavourites(productID)}
            onRemoveProductFromFavs={(productID) => removeProductFromFavourites(productID)}
            />} 
          />
        <Route 
          path="/favourites"
          element={<Favourites
            products={products.filter((product) => favourites.indexOf(product.sku) >= 0)}
            cartItems={cartItems}
            favourites={favourites}
            onAddProductToCart={(productID) => addProductToCart(productID)}
            onRemoveProductFromCart={(productID) => removeProductFromCart(productID)}
            onAddProductToFavs={(productID) => addProductToFavourites(productID)}
            onRemoveProductFromFavs={(productID) => removeProductFromFavourites(productID)}
            />}
          />
      </Routes>
    </>
  )
}