import { productTypes } from "../types";

const initialState = {
    products: [],
    cartItems: getItemsFromLocalStorage('cartItems'),
    favourites: getItemsFromLocalStorage('favourites')
};
  
function saveItemsToLocalStorage(itemKey, items) {
    window.localStorage.setItem(itemKey, JSON.stringify(items));
}
  
function getItemsFromLocalStorage(itemKey) {
    let items = [];
  
    try {
      items = JSON.parse(window.localStorage.getItem(itemKey) || '[]');
    } catch (e) {
      console.log('Inconsistent data in storage');
    }
  
    return items;
}
  
export function productsReducer(state = initialState, action) {
    switch (action.type) {
        case productTypes.GET_PRODUCTS:
            return {
                ...state,
            products: action.payload,
            };
        case productTypes.ADD_PRODUCT_TO_CART: {
            const newCartItems = [...state.cartItems, action.payload.productID];
            saveItemsToLocalStorage('cartItems', newCartItems);
            return {
                ...state,
                cartItems: newCartItems,
            };
        } 
        case productTypes.REMOVE_PRODUCT_FROM_CART: {
            const newCartItems = state.cartItems.filter((item) => item !== action.payload.productID);
            saveItemsToLocalStorage('cartItems', newCartItems);
            return {
                ...state,
                cartItems: newCartItems,
            };
        }
        case productTypes.ADD_PRODUCT_TO_FAVOURITES: {
            const newFavourites = [...state.favourites, action.payload.productID];
            saveItemsToLocalStorage('favourites', newFavourites);
            return {
                ...state,
                favourites: newFavourites,
            };
        }
        case productTypes.REMOVE_PRODUCT_FROM_FAVOURITES: {
            const newFavourites = state.favourites.filter((item) => item !== action.payload.productID);
            saveItemsToLocalStorage('favourites', newFavourites);
            return {
                ...state,
                favourites: newFavourites,
            };
        }
        default:
            return state;
    }
}
  