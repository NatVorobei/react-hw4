import { productTypes } from "../types";

export function getProducts(products){
    return {
        type: productTypes.GET_PRODUCTS,
        payload: products
    }
}

export function getProductsAsync(){
    return async function(dispatch) {
        const result = await fetch('./products.json');
        const data = await result.json();

        dispatch(getProducts(data));
    }
}

export function addToCart(productID) {
    return {
      type: productTypes.ADD_PRODUCT_TO_CART,
      payload: {
        productID
      }
    };
}

export function addToFavourites(productID) {
    return {
      type: productTypes.ADD_PRODUCT_TO_FAVOURITES,
      payload: {
        productID
      }
    };
}

export function removeFromCart(productID) {
    return {
      type: productTypes.REMOVE_PRODUCT_FROM_CART,
      payload: {
        productID
      }
    };
}

export function removeFromFavourites(productID) {
    return {
      type: productTypes.REMOVE_PRODUCT_FROM_FAVOURITES,
      payload: {
        productID
      }
    };
}
  